let fields = ['id','firstname','lastname','company','salary']; 
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

let newJson = []

for(let i in employees) {
    let objInJson = {}
    for(const a in fields) {
        objInJson[fields[a]] = employees[i][a]
        //console.log(objInJson)
        //newJson.push(objInJson)
    }
    newJson.push(objInJson)
    
}
console.log(newJson)